const express = require('express')
const app = express()
const port = 8000

app.use(express.static('public'));

// app.get('/', (req, res) => {
//     res.send('Hello, this is your simple web application!')
// })

app.listen(port, () => {
    console.log('Application is listening at https://localhost:${port}')
})